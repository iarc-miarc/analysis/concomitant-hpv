##########################################
###         Computed HPV prevalence    ###
##########################################
library(miarc.utils)
library(methis.rhea)
library(tidyverse)

# folder location #
path.to.dat<-"~/mIARC/methis/Sweden/_model"
p1<-c("HPV16","HPV18")
p2<-"routine11_cu"
p3<-c("22_35","Rou_35","C18_35")
p3.2<-c("22_35_75","Rou_35_75","C18_35_75")
p3.label<-c("Routine (1999-2007) + \nCatch-up 13-26 years (<1999)\n",
            "Routine (1999-2007) only",
            "Routine (1999-2007) + \nCatch-up 13-18 years (<1999)\n")

id<-1
for (i in 1:length(p1)){
  for (u in 1:length(p3)){
mainDir<-"~/mIARC/methis/Sweden/_model/Validation"
if (i==1) {subDir <- paste(p1[i],p2,p3[u],sep="_")}
if (i==2) {subDir <- paste(p1[i],p2,p3.2[u],sep="_")}
simu<-list.files(path=paste(mainDir,subDir,sep="/"), pattern = 'P_Tot\\(AgeGroupped\\).out$', full.names = T)

for (y in 1:length(simu)){
  data<-read.table(simu[y], sep = "\t", header = T)%>%
    filter(Year>150)%>%
    mutate(Prev=(HPV_wn__tot+HPV_wv__tot)/(tot_w),
           Age=Cal,
           Year=Year+1856,
           BirthYear=Year-Age,
           IDsim=str_split_i(str_split_i(IDsim, "_",5),"P.",1),
           Type=p1[i],
           Simu=p3.label[u])%>%
    select(IDsim, Type, Age, Year, BirthYear, Prev, Simu)
  if (id==1){
    dataset<-data
  }
  if (id>1){
    dataset<-rbind(dataset,data)
  }
  id<-id+1
}
}
}


# observed data #
obs<-readRDS(file.path(path.to.dat, 'prev.vacc.sweden.rds'))%>%
  mutate(BirthYear=Birth,
         Outcome="Observed",
         value=mean,
         LR=lower,
         UR=upper,
         Year=Birth+Age,
         Prev=value)%>%
  select(Type, Age, Year, BirthYear, value, Prev, LR, UR, Outcome)%>%
  filter(Type %in% p1)

# plot/figure #

dataset<-filter(dataset, !(Simu=="Routine (1999-2007) only" & BirthYear %in% c(1994:1998)))

P.3<-dataset %>%
  group_by(Type, Age, Year, BirthYear, Simu)%>%
  summarize(value=mean(Prev), 
            LR=quantile(Prev, 0.1),
            UR=quantile(Prev,0.9))%>%
  ungroup()%>%
  filter(BirthYear %in% c(1994:1999) & Age<30 & Age>12)%>%
  mutate(Outcome="Model")%>%
  filter(Simu!="Routine (1999-2007) + \nCatch-up 13-26 years (<1999)\n")%>%
  filter(Simu!="Routine (1999-2007) only")%>%
  ggplot(mapping = aes(x=Age, y=value))+
  geom_line(aes(x=Age, y=value, group=Simu, col=Simu),linewidth = 1) +
  geom_line(aes(x=Age, y=LR, group=Simu, col=Simu), linewidth=0.5, linetype="dashed")+
  geom_line(aes(x=Age, y=UR, group=Simu, col=Simu), linewidth=0.5, linetype="dashed")+
  geom_linerange(data=obs, aes(ymin = LR, ymax = UR),linewidth = 1)+
  geom_point(data=obs, colour = "black", fill = "white", shape = 21)+
  scale_color_manual("", values=c("#0C7BDC","#994F00","#E66100"))+
  theme_bw()+
  xlab("Age")+
  ylab("HPV prevalence")+
  ggtitle("Post-vaccination age-specific prevalence", subtitle = "By hpv type and birth cohort")+
  facet_grid(Type~BirthYear)+
  theme(legend.position = "bottom")
P.3

#ggsave(P.3, filename = file.path(path.to.dat, "Figure 2.pdf"), 
#       height=7, width=12)

