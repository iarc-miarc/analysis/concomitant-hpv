##########################################
###         Computed HPV prevalence    ###
##########################################
library(miarc.utils)
library(methis.rhea)
library(tidyverse)
library(readxl)

# folder location #
path.to.dat<-"~/mIARC/methis/Sweden/_data"
path.to.out<-"~/mIARC/methis/Sweden/_model"
p1<-c("HPV16","HPV18")
p1.l<-c("HPV 16","HPV 18")
p2<-"routine11_cu"
p3<-c("00","00_35")
p3.2<-c("00","00_35_75")
p3.l<-c("Model: published", "Model: adapted")

id<-1
for (i in 1:length(p1)){
  for (u in 1:length(p3)){
mainDir<-"~/mIARC/methis/Sweden/_model/Validation"
if (i==1){subDir <- paste(p1[i],p2,p3[u],sep="_")}
if (i==2){subDir <- paste(p1[i],p2,p3.2[u],sep="_")}
simu<-list.files(path=paste(mainDir,subDir,sep="/"), pattern = 'P_Tot\\(AgeGroupped\\).out$', full.names = T)

for (y in 1:length(simu)){
  data<-read.table(simu[y], sep = "\t", header = T)%>%
    filter(Year>150)%>%
    mutate(Prev=(HPV_wn__tot+HPV_wv__tot),
           Pop=(tot_w),
           Age=Cal,
           Year=Year+1856,
           BirthYear=Year-Age,
           Age_cut=cut(Cal, breaks=c(10,23,30,35,40,45,50,55,60,65,70,90), right=F),
           Age=paste(substr(Age_cut,2,3),"-",as.numeric(substr(Age_cut,5,6))-1,sep=""),
           IDsim=str_split_i(str_split_i(IDsim, "_",5),"P.",1),
           Type=p1.l[i], 
           Outcome=p3.l[u])%>%
    select(IDsim, Type, Age, Cal, Year, BirthYear, Prev, Pop, Outcome)
  if (id==1){
    dataset<-data
  }
  if (id>1){
    dataset<-rbind(dataset,data)
  }
  id<-id+1
}
}
}
# observed data #
obs<-read_excel(file.path(path.to.dat, 'Cobas_Data.xlsx'), sheet="Data")%>%
  mutate(BirthYear=1994,
         Outcome="Observed",
         value=PCT_ROW,
         LR=pct_lcl,
         UR=pct_ucl,
         Year=2021,
         Prev=value,
         Type=HPV_hierachy,
         Age=age_cat)%>%
  select(Type, Age, Year, BirthYear, value, Prev, LR, UR, Outcome)%>%
  filter(Type %in% p1.l)%>%
  select(Age, value, LR, UR, BirthYear, Type, Outcome)

# plot/figure #

# mean value + range #
data.figure<-dataset %>%
  filter(Cal %in% c(23:64))%>%
  group_by(Type, Age, BirthYear, Outcome, IDsim)%>%
  summarise(Prev=sum(Prev), Pop=sum(Pop))%>%
  mutate(Prev=(Prev/Pop)*100)%>%
  ungroup()%>%
  group_by(Type, Age, BirthYear, Outcome)%>%
  summarize(value=mean(Prev), LR=min(Prev),UR=max(Prev))%>%
  ungroup()%>%
  filter(BirthYear %in% c(1994))%>%
  select(Age, value, LR, UR, BirthYear, Type, Outcome)

data.figure<-rbind(data.figure, obs)

  
P.2<-ggplot(data=filter(data.figure, 
                        #!(Outcome %in% c("Model: 30% high","Model: 35% high","Model: 50% high"))&
                        !(Age %in% c("55-59","60-64"))), mapping = aes(x=Age, y=value, ymin = LR, ymax = UR, group=Outcome, col=Outcome, shape=Outcome))+
  geom_linerange(aes(ymin = LR, ymax = UR), position=position_dodge(width = 0.6))+
  geom_point(aes(fill=Outcome),position=position_dodge(width = 0.6), show.legend = T)+
  theme_bw()+
  xlab("")+
  ylab("HPV prevalence (Pre vaccination period)")+
  scale_color_manual("", values=c("#0C7BDC","#DC3220","#191919"))+
  scale_fill_manual("", values=c("#0C7BDC","#DC3220","#191919"))+
  scale_shape_manual("", values=c(21,21,22))+
  facet_wrap(~Type)+ggtitle("Pre-vaccination age-specific prevalence", subtitle = "By hpv type")
P.2

#ggsave(P.2, filename = file.path(path.to.out, "Figure 1.pdf"), 
#       height=7, width=12)

