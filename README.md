# concomitant HPV

This webpage reports the codes used for analysing the impact of Concomitant Human Papillomavirus (HPV) Vaccination and HPV Screening. 

The results of this analysis are included in a researc work by Arroyo et al. The manuscript can be found at the following link: https://www.nature.com/articles/s41467-024-47909-x

## Description
This webpage is structured as follow:
- _code reports the R script used to produce the manuscript modelling results
- _model reports an example of model input and output
- _software reports the compile software used to simulate HPV transmission

## Please note
the model used in this analysis is one element of the METHIS (ModElling Tools for HPV Infection-related cancers) project, which will gradually make publicly available a set of open-source models on a platform at the webpage https://iarc-miarc.gitlab.io/methis/methis.website from now to 2025.